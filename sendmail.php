<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/Exception.php';


class Mail
{

    public function send_Mail()
    {

        if (!empty($_POST)) {
            $data = $_POST;

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && empty($this->validate($data))) {
                $mail = new PHPMailer(true);
                $mail->CharSet = 'UTF-8';
                $mail->setLanguage('en', 'PHPMailer/language/');
                $mail->IsHTML(true);

                $mail->setFrom(htmlspecialchars(trim($_POST['Email'])));
                $mail->addAddress('kav@webernetic.by');
                $mail->Subject = 'letter from client';


                $body = '<h1>letter from client</h1>';

                if (trim(!empty($_POST['name']))) {
                    $body .= '<p><strong>Name:</strong>' . htmlspecialchars(trim($_POST['name'])) . '</p>';
                }

                if (trim(!empty($_POST['lastName']))) {
                    $body .= '<p><strong>Last Name:</strong>' . htmlspecialchars(trim($_POST['lastName'])) . '</p>';
                }
                if (trim(!empty($_POST['Email']))) {
                    $body .= '<p><strong>Email:</strong>' . htmlspecialchars(trim($_POST['Email'])) . '</p>';
                }
                $body .= '<p><strong>Phone:</strong>' . htmlspecialchars(trim($_POST['phone'])) . '</p>';
                $body .= '<p><strong>Message:</strong>' . htmlspecialchars(trim($_POST['message'])) . '</p>';

                $mail->Body = $body;

                if (!$mail->send()) {
                    $message = 'Ошибка';
                } else {
                    $message = 'Данные отправлены!';
                }
                $response = ['success' => $message];
                header('Content-type: application/json');
                echo json_encode($response);
            } else {
                $response = ['errors' => $this->validate($data)];
                echo json_encode($response);
            }
        }
    }
    public function validate($data)
    {
        $errors = [];
        if (isset($data['name'])) {
            if (mb_strlen(trim($data['name'])) < 3 || mb_strlen(trim($data['name'])) > 32) {
                $errors['name'] = 'Поля "Name" заполнено некорректно';
            }
        }
        if (isset($data['lastName'])) {
            if (mb_strlen(trim($data['lastName'])) < 3 || mb_strlen(trim($data['lastName'])) > 32) {
                $errors['lastName'] = 'Поля "Last Name" заполнено некорректно';
            }
        }
        if (isset($data['Email'])) {
            if (!preg_match('/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/', $data['Email'])) {
                $errors['Email'] = 'Введите E-mail в праильном формате';
            }
        }
        if (isset($data['phone']) && $data['phone'] != '') {
            if (!preg_match('/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){9,14}(\s*)?$/', $data['phone'])) {
                $errors['phone'] = 'Некорректно введен номер телефона';
            }
        }
        return $errors;
    }

}

$mail = new Mail();
$mail->send_Mail();
