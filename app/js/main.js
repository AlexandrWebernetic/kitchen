"use strict"


document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('form'),
        popup = document.getElementsByClassName('form-popup'),
        btnClose = document.getElementById('btn-close');

    btnClose.addEventListener('click',function () {
        $(popup).removeClass('active');
    });
    form.addEventListener('submit', formSend);

    async function formSend(e) {
        e.preventDefault();
        let error = formValidate(form);
        let formData = new FormData(form);
        if (error === 0) {
            form.classList.add('sending');
            let response = await fetch('../sendmail.php', {
                method: 'POST',
                body: formData,
            });
            if (response.ok) {
                let result = await response.json();
                if (result['success']) {
                    $(popup).addClass('active').find('.form-popup__box-text').text(result['success']);
                    form.reset();
                    form.classList.remove('sending');
                } else if (result['errors']) {
                    $.map(result['errors'], function (value, key) {
                        $(form).find('input').parent().addClass('success');
                        $(form).find('[name = "' + key + '"]').parent().remove('success').addClass('error').children('span').text(value);
                    });
                    form.classList.remove('sending');
                }
            } else {
                form.classList.remove('sending');
            }
        }
    }

    function formValidate(form) {

        let error = 0;
        let formReq = document.querySelectorAll('._req');
        for (let index = 0; index < formReq.length; index++) {
            const input = formReq[index];
            formRemoveError(input);
            if (input.classList.contains('email')) {
                if (emailTest(input)) {
                    formAddError(input);
                    error++;
                }
            } else if (input.classList.contains('validName') && input.value === '') {
                formAddError(input);
                error++;
            } else if (input.classList.contains('phoneNumber')) {
                if (phoneNumber(input)) {
                    formAddError(input);
                    error++;
                }
            }
        }
        return error;
    }


    function formAddError(input) {
        input.classList.add('error');
        input.parentElement.classList.add('error');
    }


    function formRemoveError(input) {
        input.classList.remove('error');
        input.parentElement.classList.remove('error');
    }

    function emailTest(input) {
        return !/^\w+([\.-]?\w+)*@\w+([\.-]?w+)*(\.\w{2,8})+$/.test(input.value);
    }

    function phoneNumber(input) {
        return !/^\d+$/.test(input.value);
    }

});